﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesExample
{//1.create the dalegate type
    delegate void PrecentHandler(int precent);
    class NassaCalculator
    {
        //2.create avariable from the elegate
        public int Calculate(PrecentHandler funcAddress)
        {
            int sum = 0;
            for (int i = 1; i < 200; i++)
            {
                sum += i;
                Thread.Sleep(200);
                //3.call the delegate function
                funcAddress(i);
                //Console.Write("\r" + i + "%");
            }
            return sum;
        }
    }
}
